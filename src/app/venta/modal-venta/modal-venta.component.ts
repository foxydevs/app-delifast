import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { VentaService } from 'src/app/_service/venta.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-modal-venta',
  templateUrl: './modal-venta.component.html',
  styleUrls: ['./modal-venta.component.scss'],
})
export class ModalVentaComponent implements OnInit {
  total:number = 0;
  data:any;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private notificationService: NotificacionService,
    private mainService: VentaService
  ) { }

  ngOnInit() {
    this.getSingle(this.navParams.get('value'))
  }

  closeModal() {
    this.modalController.dismiss();
  }

  getSingle = (id:number) => {
    this.notificationService.alertLoading('Cargando...', 1000);
    this.mainService.getSingle(id)
    .subscribe((res)=>{
      console.log(res)
      this.data = res;
      res.detalle.forEach(element => {
        console.log(element)
        this.total = this.total + element.subtotal;
      });
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();      
    });
  }

}
