import { Component, OnInit } from '@angular/core';
import { NotificacionService } from '../_service/notificacion.service';
import { VentaService } from '../_service/venta.service';
import { ModalController, AlertController } from '@ionic/angular';
import { ModalVentaComponent } from './modal-venta/modal-venta.component';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.page.html',
  styleUrls: ['./venta.page.scss'],
})
export class VentaPage implements OnInit {
  search:string;
  table:any[];
  selectItem:string = 'venta';

  constructor(
    private notificationService: NotificacionService,
    private mainService: VentaService,
    private modalController: ModalController,
    private router: Router,
    private location: Location,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
    //console.log('Segment changed', ev);
    if(ev.detail.value == 'venta') {
      this.getAll();
      //this.getAllPromocion();
    } else if(ev.detail.value == 'anulada') {
      //this.getAllDescuento();
      this.getAllAnuladas();
    }
  }

  getAll = () => {
    this.notificationService.alertLoading('Cargando...', 1000);
    this.mainService.getAll()
    .subscribe((res)=>{
      console.log(res)
      this.table = [];
      this.table = res;
      this.table.reverse();
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();      
    });
  }

  getAllAnuladas = () => {
    this.notificationService.alertLoading('Cargando...', 1000);
    this.mainService.getAllAnuladas()
    .subscribe((res)=>{
      console.log(res)
      this.table = [];
      this.table = res;
      this.table.reverse();
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();      
    });
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalVentaComponent,
      componentProps: {
        value: id
      }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
    });
    return await modal.present();
  }

  async presentAlertConfirm(id:any) {
    const alert = await this.alertController.create({
      header: 'Venta',
      message: '¿Deseas anular la venta?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.delete(id)
          }
        }
      ]
    });

    await alert.present();
  }

  delete(id:any) {
    let data = {
      id: id,
      estado: 0
    }
    this.mainService.update(data)
    .subscribe((res) => {
      this.notificationService.alertMessage('Venta Anulada', 'La venta fue anulada exitosamente.');
      this.getAll();
    }, (error) => {
      console.clear
    });
  }

}
