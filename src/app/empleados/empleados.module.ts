import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmpleadosPageRoutingModule } from './empleados-routing.module';

import { EmpleadosPage } from './empleados.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ModalEmpleadosComponent } from './modal-empleados/modal-empleados.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    EmpleadosPageRoutingModule
  ],
  declarations: [
    EmpleadosPage,
    ModalEmpleadosComponent
  ], entryComponents: [
    ModalEmpleadosComponent
  ]
})
export class EmpleadosPageModule {}
