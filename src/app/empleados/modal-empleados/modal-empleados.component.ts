import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { EmpleadoService } from 'src/app/_service/empleado.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { EmpleosService } from 'src/app/_service/empleos.service';
import { SucursalService } from 'src/app/_service/sucursal.service';

@Component({
  selector: 'app-modal-empleados',
  templateUrl: './modal-empleados.component.html',
  styleUrls: ['./modal-empleados.component.scss'],
})
export class ModalEmpleadosComponent implements OnInit {
  puestos:any[];
  sucursales:any[];
  parameter:string;
  data = {
    nombre: '',
    apellido: '',
    direccion: '',
    telefono: '',
    celular: '',
    sueldo: '',
    sucursal: '',
    puesto: '',
    id: ''
  }
  btnDisabled:boolean;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private mainService: EmpleadoService,
    private secondService: EmpleosService,
    private thirdService: SucursalService,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.getAllSecond();
    this.getAllThird();
    this.parameter = this.navParams.get('value')
    if(this.parameter) {
      this.getSingle(+this.parameter);
    }
  }

  getAllSecond() {
    this.secondService.getAll()
    .subscribe((res) => {
      this.puestos = [];
      this.puestos = res;
      console.log(res);
    }, (error) => {
      console.log(error);
    })
  }

  getAllThird() {
    this.thirdService.getAll()
    .subscribe((res) => {
      this.sucursales = [];
      this.sucursales = res;
      console.log(res);
    }, (error) => {
      console.log(error);
    })
  }

  closeModal() {
    this.modalController.dismiss();
  }

    //GUARDAR CAMBIOS
  saveChanges() {
    if(this.data.nombre) {
      if(this.data.apellido) {
        if(this.data.direccion) {
          if(this.data.telefono) {
            if(this.parameter) {
              this.btnDisabled = true;
              this.update(this.data);
            } else {
              this.btnDisabled = true;
              this.create(this.data);
            }
          } else {
            this.notificationService.alertToast('El telefono es requerido.');
          }
        } else {
          this.notificationService.alertToast('La direccion es requerida.');
        }
      } else {
        this.notificationService.alertToast('El apellido es requerido.');
      }
    } else {
      this.notificationService.alertToast('El nombre es requerido.');
    }
  }

  getSingle(id:number) {
    this.mainService.getSingle(id)
    .subscribe(res => {
      this.data = res;
    }, error => {
      console.log(error)
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Empleado Agregado', 'El Empleado fue agregado exitosamente.');
      this.modalController.dismiss(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Empleado Actualizado', 'El Empleado fue actualizado exitosamente.');
      this.modalController.dismiss(res);
      console.log(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

}
