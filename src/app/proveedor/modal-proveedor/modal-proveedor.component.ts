import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ProveedorService } from 'src/app/_service/proveedor.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-modal-proveedor',
  templateUrl: './modal-proveedor.component.html',
  styleUrls: ['./modal-proveedor.component.scss'],
})
export class ModalProveedorComponent implements OnInit {
  parameter:string;
  provider = {
    nombre: '',
    direccion: '',
    nit: '',
    telefono: '',
    cuenta: '',
    id: ''
  }
  btnDisabled:boolean;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private mainService: ProveedorService,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value')
    if(this.parameter) {
      this.getSingle(+this.parameter);
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

    //GUARDAR CAMBIOS
  saveChanges() {
    if(this.provider.nombre) {
      if(this.provider.direccion) {
        if(this.provider.nit) {
          if(this.provider.telefono) {
            if(this.parameter) {
              this.btnDisabled = true;
              this.update(this.provider);
            } else {
              this.btnDisabled = true;
              this.create(this.provider);
            }
          } else {
            this.notificationService.alertToast('El telefono es requerido.');
          }
        } else {
          this.notificationService.alertToast('El nit es requerido.');
        }
      } else {
        this.notificationService.alertToast('La dirección es requerida.');
      }
    } else {
      this.notificationService.alertToast('El nombre es requerido.');
    }
  }

  getSingle(id:number) {
    this.mainService.getSingle(id)
    .subscribe(res => {
      this.provider = res;
    }, error => {
      console.log(error)
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Proveedor Agregado', 'El Proveedor fue agregado exitosamente.');
      this.modalController.dismiss(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Proveedor Actualizado', 'El Proveedor fue actualizado exitosamente.');
      this.modalController.dismiss(res);
      console.log(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

}
