import { Component } from '@angular/core';

import { Platform, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public authentication:any;
  public appPages = [];
  data = {
    picture: 'http://foxylabs.xyz/Documentos/imgs/logo.png',
    rol: '',
    email: ''
  }
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private events: Events
  ) {
    this.initializeApp();
    events.subscribe('user:login', res => {
      this.authentication = localStorage.getItem('currentAuthentication');
      this.getData();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.authentication = localStorage.getItem('currentAuthentication');
      this.getData();
    });
  }

  public getMenu() {
    this.appPages = [];
    this.appPages.push({
      title: 'Inicio',
      url: '/home',
      icon: 'home'
    });
    this.appPages.push({
      title: 'Sucursales',
      url: '/sucursales',
      icon: 'map'
    });
    this.appPages.push({
      title: 'Empleados',
      url: '/empleados',
      icon: 'person'
    });
    this.appPages.push({
      title: 'Compras',
      url: '/compra',
      icon: 'cash'
    });
    this.appPages.push({
      title: 'Ventas',
      url: '/venta',
      icon: 'card'
    });
    this.appPages.push({
      title: 'Inventario',
      url: '/inventario',
      icon: 'list'
    });
    this.appPages.push({
      title: 'Clientes',
      url: '/clientes',
      icon: 'people'
    });
    this.appPages.push({
      title: 'Proveedores',
      url: '/proveedores',
      icon: 'business'
    });
    this.appPages.push({
      title: 'Cuentas por Cobrar',
      url: '/cuenta-cobrar',
      icon: 'document'
    });
    this.appPages.push({
      title: 'Cuentas por Pagar',
      url: '/cuenta-pagar',
      icon: 'document'
    });
  }

  logOut() {
    this.appPages = [];
    localStorage.clear();
    localStorage.setItem('currentAuthentication', 'NoAuthentication');
    this.authentication = localStorage.getItem('currentAuthentication');
    this.data.picture = localStorage.getItem('currentPicture')
    this.data.rol = localStorage.getItem('currentRol')
    this.data.email = localStorage.getItem('currentEmail')
  }

  getData() {
    if(localStorage.getItem('currentAuthentication') == 'Authentication') {
      this.data.picture = localStorage.getItem('currentPicture')
      this.data.rol = localStorage.getItem('currentRol')
      this.data.email = localStorage.getItem('currentEmail')
      this.getMenu()
    } else {
      this.data.picture = localStorage.getItem('currentPicture')
      this.data.rol = localStorage.getItem('currentRol')
      this.data.email = localStorage.getItem('currentEmail')
    }
  }  

}
