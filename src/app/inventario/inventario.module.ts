import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InventarioPageRoutingModule } from './inventario-routing.module';

import { InventarioPage } from './inventario.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ModalInventarioComponent } from './modal-inventario/modal-inventario.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InventarioPageRoutingModule,
    Ng2SearchPipeModule
  ],
  declarations: [
    InventarioPage,
    ModalInventarioComponent
  ], entryComponents: [
    ModalInventarioComponent
  ]
})
export class InventarioPageModule {}
