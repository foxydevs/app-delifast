import { Component, OnInit } from '@angular/core';
import { ProductoTipoService } from 'src/app/_service/producto-tipo.service';
import { InventarioService } from 'src/app/_service/inventario.service';
import { ModalController, NavParams } from '@ionic/angular';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-modal-inventario',
  templateUrl: './modal-inventario.component.html',
  styleUrls: ['./modal-inventario.component.scss'],
})
export class ModalInventarioComponent implements OnInit {
  private typesProducts:any[] = []
  private parameter:any;
  private title:any;
  private stocktaking = {
    producto: '',
    id: '',
    codigo: '',
    nombre: '',
    descripcion: '',
    marcaDes: '',
    tipo: '',
    minimo: '',
    descuento: '',
    precioCosto: '',
    precioVenta: '',
    cantidad: '',
    precioClienteEs: 0,
    precioDistribuidor: 0
  }
  btnDisabled:boolean;

  constructor(
  public secondService: ProductoTipoService,
  public mainService: InventarioService,
  public notificationService: NotificacionService,
  public modalController: ModalController,
  public navParams: NavParams) {
    this.parameter = this.navParams.get('value')
    this.getAllSecond();
    if(this.parameter) {
      this.getSingle(this.parameter);
    } else {

    }
  }

  //CERRAR MODAL
  public closeModal() {
    this.modalController.dismiss();
  }

  ngOnInit() {}

  //CARGAR TIPOS DE PRODUCTO
  public getAllSecond() {
    this.secondService.getAll()
    .subscribe(res => {
      this.typesProducts = res;
    }, (error) => {
      console.log(error)
    })
  }

  saveChanges() {
    if(this.stocktaking.codigo) {
      if(this.stocktaking.nombre) {
        if(this.stocktaking.marcaDes) {
          if(this.stocktaking.tipo) {
            if(this.stocktaking.cantidad) {
              if(this.stocktaking.precioCosto) {
                if(this.stocktaking.precioVenta) {
                  this.btnDisabled = true;
                  if(this.parameter) {
                    this.update(this.stocktaking)
                  } else {
                    this.create(this.stocktaking)
                  }
                } else {
                  this.notificationService.alertToast('El precio de Venta es requerido.');
                }
              } else {
                this.notificationService.alertToast('El Precio de Costo es requerido.');
              }
            } else {
              this.notificationService.alertToast('La cantidad es requerida.');
            }
          } else {
            this.notificationService.alertToast('El tipo es requerido.');
          }
        } else {
          this.notificationService.alertToast('La marca es requerida.');
        }
      } else {
        this.notificationService.alertToast('El nombre es requerido.');
      }
    } else {
      this.notificationService.alertToast('El código es requerido.');
    }
  }

  //LOAD DATA
  public getSingle(id:any) {
    this.mainService.getSingle(id)
    .subscribe(res => {
      console.log(res)
      this.stocktaking.cantidad = res.cantidad;
      this.stocktaking.codigo = res.productos.codigo;
      this.stocktaking.descripcion = res.productos.descripcion;
      this.stocktaking.descuento = res.descuento;
      this.stocktaking.producto = res.productos.id;
      this.stocktaking.id = res.id;      
      this.stocktaking.marcaDes = res.productos.marcaDes;
      this.stocktaking.minimo = res.minimo;
      this.stocktaking.nombre = res.productos.nombre;
      this.stocktaking.tipo = res.productos.tipo;
      this.stocktaking.precioClienteEs = res.precioClienteEs;
      this.stocktaking.precioCosto = res.precioCosto;
      this.stocktaking.precioDistribuidor = res.precioDistribuidor;
      this.stocktaking.precioVenta = res.precioVenta;
    }, (error) => {
      console.log(error);
    })
  }

  public calcPrice() {
    this.stocktaking.precioClienteEs = +this.stocktaking.precioVenta -(+this.stocktaking.precioVenta*0.15)
    this.stocktaking.precioDistribuidor = +this.stocktaking.precioVenta -(+this.stocktaking.precioVenta*0.20)
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Producto Agregado', 'El producto fue agregado exitosamente.');
      this.modalController.dismiss(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Producto Actualizado', 'El producto fue actualizado exitosamente.');
      this.modalController.dismiss(res);
      console.log(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

}
