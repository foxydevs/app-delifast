import { Component, OnInit } from '@angular/core';
import { InventarioService } from '../_service/inventario.service';
import { ModalController, AlertController } from '@ionic/angular';
import { ModalInventarioComponent } from './modal-inventario/modal-inventario.component';
import { NotificacionService } from '../_service/notificacion.service';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.page.html',
  styleUrls: ['./inventario.page.scss'],
})
export class InventarioPage implements OnInit {
  selectItem:string = 'inventario';
  table:any[];

  constructor(
    private mainService: InventarioService,
    private modalController: ModalController,
    private notificationService: NotificacionService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.getAll();
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
    if(ev.detail.value == 'inventario') {
      this.getAll();
    } else if(ev.detail.value == 'administrador') {
      this.getAllAdmin();
    } else if(ev.detail.value == 'inicial') {
      this.getAll();
    }
  }

  //CARGAR TODO
  public getAll() {
    this.mainService.getAll()
    .subscribe(res => {
      console.log(res)
      this.table = []
      this.table = res
    }, error => {
      console.log(error)}
    )
  }

  public getAllAdmin() {
    this.mainService.getAllAdmin()
    .subscribe(res => {
      console.log(res)
      this.table = []
      this.table = res
    }, error => {
      console.log(error)}
    )
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalInventarioComponent,
      componentProps: {
        value: id
      }
    });

    modal.onDidDismiss().then((data: any) => {
      if(data.data) {
        this.getAll();
      }
    });

    return await modal.present();
  }

  async presentAlertConfirm(id:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Deseas eliminar el producto?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.delete(id)
          }
        }
      ]
    });

    await alert.present();
  }

  delete(id:any) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Producto Eliminado', 'El producto fue eliminado exitosamente.');
      this.getAll();
    }, (error) => {
      console.clear
    });
  }

}
