import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CompraPageRoutingModule } from './compra-routing.module';

import { CompraPage } from './compra.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ModalCompraComponent } from './modal-compra/modal-compra.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    CompraPageRoutingModule
  ],
  declarations: [
    CompraPage
  ], entryComponents: [
  ]
})
export class CompraPageModule {}
