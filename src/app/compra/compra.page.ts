import { Component, OnInit } from '@angular/core';
import { CompraService } from '../_service/compra.service';
import { NotificacionService } from '../_service/notificacion.service';
import { ModalController, AlertController } from '@ionic/angular';
import { ModalCompraComponent } from './modal-compra/modal-compra.component';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.page.html',
  styleUrls: ['./compra.page.scss'],
})
export class CompraPage implements OnInit {
  search:string;
  table:any[];
  selectItem:string = 'compra';

  constructor(
    private mainService: CompraService,
    private notificationService: NotificacionService,
    private modalController: ModalController,
    private router: Router,
    private location: Location,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.getAll();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
    //console.log('Segment changed', ev);
    if(ev.detail.value == 'compra') {
      //this.getAllPromocion();
      this.getAll();
    } else if(ev.detail.value == 'anulada') {
      //this.getAllDescuento();
      this.getAllAnuladas();
    }
  }

  getAll = () => {
    this.notificationService.alertLoading('Cargando...', 1000);
    this.mainService.getAll()
    .subscribe((res)=>{
      console.log(res)
      this.table = [];
      this.table = res;
      this.table.reverse();
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();      
    });
  }

  getAllAnuladas = () => {
    this.notificationService.alertLoading('Cargando...', 1000);
    this.mainService.getAllAnuladas()
    .subscribe((res)=>{
      console.log(res)
      this.table = [];
      this.table = res;
      this.table.reverse();
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();      
    });
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalCompraComponent,
      componentProps: {
        value: id
      }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
    });
    return await modal.present();
  }

  async presentAlertConfirm(id:any) {
    const alert = await this.alertController.create({
      header: 'Compra',
      message: '¿Deseas anular la compra?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.delete(id)
          }
        }
      ]
    });

    await alert.present();
  }

  delete(id:any) {
    let data = {
      id: id,
      estado: 0
    }
    this.mainService.update(data)
    .subscribe((res) => {
      this.notificationService.alertMessage('Compra Anulada', 'La compra fue anulada exitosamente.');
      this.getAll();
    }, (error) => {
      console.clear
    });
  }

}
