import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { CompraService } from 'src/app/_service/compra.service';

@Component({
  selector: 'app-modal-compra',
  templateUrl: './modal-compra.component.html',
  styleUrls: ['./modal-compra.component.scss'],
})
export class ModalCompraComponent implements OnInit {
  private total:number = 0;
  data:any;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private notificationService: NotificacionService,
    private mainService: CompraService
  ) { }

  ngOnInit() {
    this.getSingle(this.navParams.get('value'))
  }

  closeModal() {
    this.modalController.dismiss();
  }

  getSingle = (id:number) => {
    this.notificationService.alertLoading('Cargando...', 1000);
    this.mainService.getSingle(id)
    .subscribe((res)=>{
      console.log(res)
      this.data = res;
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();      
    });
  }
}
