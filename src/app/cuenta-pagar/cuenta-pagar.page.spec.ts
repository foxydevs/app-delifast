import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CuentaPagarPage } from './cuenta-pagar.page';

describe('CuentaPagarPage', () => {
  let component: CuentaPagarPage;
  let fixture: ComponentFixture<CuentaPagarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuentaPagarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CuentaPagarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
