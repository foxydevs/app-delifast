import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CuentaPagarPageRoutingModule } from './cuenta-pagar-routing.module';

import { CuentaPagarPage } from './cuenta-pagar.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    CuentaPagarPageRoutingModule
  ],
  declarations: [CuentaPagarPage]
})
export class CuentaPagarPageModule {}
