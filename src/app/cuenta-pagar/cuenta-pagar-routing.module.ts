import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CuentaPagarPage } from './cuenta-pagar.page';

const routes: Routes = [
  {
    path: '',
    component: CuentaPagarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CuentaPagarPageRoutingModule {}
