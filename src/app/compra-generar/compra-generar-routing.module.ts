import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompraGenerarPage } from './compra-generar.page';

const routes: Routes = [
  {
    path: '',
    component: CompraGenerarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompraGenerarPageRoutingModule {}
