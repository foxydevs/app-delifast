import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CompraGenerarPage } from './compra-generar.page';

describe('CompraGenerarPage', () => {
  let component: CompraGenerarPage;
  let fixture: ComponentFixture<CompraGenerarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompraGenerarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CompraGenerarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
