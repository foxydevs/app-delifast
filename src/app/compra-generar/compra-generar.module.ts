import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CompraGenerarPageRoutingModule } from './compra-generar-routing.module';

import { CompraGenerarPage } from './compra-generar.page';
import { ModalCompraProductComponent } from './modal-compra-product/modal-compra-product.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CompraGenerarPageRoutingModule
  ],
  declarations: [
    CompraGenerarPage,
    ModalCompraProductComponent
  ], entryComponents: [
    ModalCompraProductComponent
  ]
})
export class CompraGenerarPageModule {}
