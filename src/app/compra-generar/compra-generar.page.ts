import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ProveedorService } from '../_service/proveedor.service';
import { CompraService } from '../_service/compra.service';
import { CompraTipoService } from '../_service/compra-tipo.service';
import { NotificacionService } from '../_service/notificacion.service';
import { ModalCompraProductComponent } from './modal-compra-product/modal-compra-product.component';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ClienteComponent } from '../cliente/cliente.component';
import { ProveedorComponent } from '../proveedor/proveedor.component';

@Component({
  selector: 'app-compra-generar',
  templateUrl: './compra-generar.page.html',
  styleUrls: ['./compra-generar.page.scss'],
})
export class CompraGenerarPage implements OnInit {
  typesSales:any[] = [];
  products:any[] = [];
  selectItem:String = 'detalle';
  purchase = {
    nit: '',
    nombre: '',
    direccion: '',
    proveedor: '',
    fecha: '',
    comprobante: 0,
    tipo: '',
    tipoPlazo: '',
    plazo: '',
    total: 0,
    detalle: [],
    usuario: +localStorage.getItem('currentId')
  }
  public parameter:any;
  private now:any;

  constructor(
    public compraTipoService: CompraTipoService,
    public proveedorService: ProveedorService,
    public compraService: CompraService,
    public notificationService: NotificacionService,
    public modalController: ModalController,
    private router: Router,
    private location: Location
  ) {
    this.getAllTipoCompra();
    this.loadInvoice();
    let date = new Date();
    let month = date.getMonth()+1;
    let month2;
    let dia= date.getDate();
    let dia2;
    if(month<10){
      month2='0'+month;
    }else{
      month2=month
    }
    if(dia<10){
      dia2='0'+dia;
    }else{
      dia2=dia
    }
    this.now= date.getFullYear()+'-'+month2+'-'+dia2
    this.purchase.fecha = this.now;
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
  }

  ngOnInit() {
  }

  //CARGAR VENTAS
  public getAllTipoCompra(){
    this.compraTipoService.getAll()
    .subscribe(res => {
      this.typesSales = res;
      console.log(this.typesSales)
    }, (error) => {
      console.clear
    });
  }

  //CARGAR PROVEEDOR
  public getSingleProvider(id:any) {
    this.proveedorService.getSingle(id)
    .subscribe(res => {
      this.purchase.proveedor = res.id;
      this.purchase.nit = res.nit;
      this.purchase.direccion = res.direccion;
      this.purchase.nombre = res.nombre;
    }, (error) => {
      console.clear
    });
  }

  //CARGAR FACTURA
  public loadInvoice(){
    this.compraService.getAll()
    .subscribe(res => {
      let num:any[] = [];
      num = res;
      num.reverse()
      this.purchase.comprobante = +num[0].id + 1;
    }, (error) => {
      console.clear
    })
  }

  async presentModalClient() {
    const modal = await this.modalController.create({
      component: ProveedorComponent
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
      if(data.data) {
        this.getSingleProvider(data.data);
        this.purchase.tipo = '1';
      }
    });
    return await modal.present();
  }

  async presentModalProducts() {
    const modal = await this.modalController.create({
      component: ModalCompraProductComponent
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
      if(data.data) {
        let a:number = 0;
        let b:number = 0;
        this.parameter = data.data;
        this.products.push(this.parameter);
        for(let x of this.products) {
          a = x.subtotal;
          b += a;
        }
        this.purchase.total = b;
        console.log(this.products)
      }
    });
    return await modal.present();
  }

  //REMUEVE EL PRODUCTO
  public removeAddItem(e:any):any {
    this.purchase.total -= e.subtotal;
    this.products.splice(this.products.indexOf(e),1)
  }

  saveChanges() {
    this.purchase.detalle = this.products;
    if(this.purchase.detalle.length > 0) {
      if(this.purchase.nit) {
        if(this.purchase.tipo) {
          this.savePurchase();
        } else {
          this.notificationService.alertToast('El tipo es requerido,')
        }
      } else {
        this.notificationService.alertToast('El proveedor es requerido.')
      }
    } else {
      this.notificationService.alertToast('El detalle es requerido.')
    }
  }

  //AGREGAR INVENTARIO
  public savePurchase() {
    if(this.purchase.detalle.length > 0) {
      this.compraService.create(this.purchase)
      .subscribe((res) => {
        //this.navCtrl.setRoot(PurchaseAdminPage)
        this.goToRoute('compra')
      }, (error) => {
        console.log(error)
      })
    } else {
      this.notificationService.alertToast('El detalle es requerido.')
    }
  }

}
