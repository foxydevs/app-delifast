import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SucursalesPageRoutingModule } from './sucursales-routing.module';

import { SucursalesPage } from './sucursales.page';
import { ModalSucursalComponent } from './modal-sucursal/modal-sucursal.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    SucursalesPageRoutingModule
  ],
  declarations: [
    SucursalesPage,
    ModalSucursalComponent
  ], entryComponents: [
    ModalSucursalComponent
  ]
})
export class SucursalesPageModule {}
