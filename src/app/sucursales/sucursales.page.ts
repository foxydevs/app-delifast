import { Component, OnInit } from '@angular/core';
import { SucursalService } from '../_service/sucursal.service';
import { ModalController, AlertController } from '@ionic/angular';
import { NotificacionService } from '../_service/notificacion.service';
import { ModalSucursalComponent } from './modal-sucursal/modal-sucursal.component';

@Component({
  selector: 'app-sucursales',
  templateUrl: './sucursales.page.html',
  styleUrls: ['./sucursales.page.scss'],
})
export class SucursalesPage implements OnInit {
  table:any[];

  constructor(
    private mainService: SucursalService,
    private modalController: ModalController,
    private alertController: AlertController,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.getAll();
  }

  closeModal() {
    this.modalController.dismiss();
  }

  getAll() {
    this.mainService.getAll()
    .subscribe((res) => {
      this.table = [];
      this.table = res;
      console.log(res);
    }, (error) => {
      console.log(error);
    })
  }

  async presentAlertConfirm(id:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Deseas eliminar el sucursal?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.delete(id)
          }
        }
      ]
    });

    await alert.present();
  }

  delete(id:any) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Sucursal Eliminado', 'El sucursal fue eliminado exitosamente.');
      this.getAll();
    }, (error) => {
      console.clear
    });
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalSucursalComponent,
      componentProps: {
        value: id
      }
    });

    modal.onDidDismiss().then((data: any) => {
      if(data.data) {
        this.getAll();
      }
    });

    return await modal.present();
  }

}
