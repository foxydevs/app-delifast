import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { SucursalService } from 'src/app/_service/sucursal.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-modal-sucursal',
  templateUrl: './modal-sucursal.component.html',
  styleUrls: ['./modal-sucursal.component.scss'],
})
export class ModalSucursalComponent implements OnInit {
  parameter:string;
  data = {
    nombre: '',
    descripcion: '',
    direccion: '',
    nit: '',
    codigo: '',
    telefono: '',
    id: ''
  }
  btnDisabled:boolean;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private mainService: SucursalService,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value')
    if(this.parameter) {
      this.getSingle(+this.parameter);
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

    //GUARDAR CAMBIOS
  saveChanges() {
    if(this.data.nombre) {
      if(this.data.descripcion) {
        if(this.data.direccion) {
          if(this.data.nit) {
            if(this.data.codigo) {
              if(this.data.telefono) {
                if(this.parameter) {
                  this.btnDisabled = true;
                  this.update(this.data);
                } else {
                  this.btnDisabled = true;
                  this.create(this.data);
                }
              } else {
                this.notificationService.alertToast('El telefono es requerido.');
              }
            } else {
              this.notificationService.alertToast('El codigo es requerido.');
            }
          } else {
            this.notificationService.alertToast('El nit es requerido.');
          }
        } else {
          this.notificationService.alertToast('La dirección es requerida.');
        }
      } else {
        this.notificationService.alertToast('La descripcion es requerida.');
      }
    } else {
      this.notificationService.alertToast('El nombre es requerido.');
    }
  }

  getSingle(id:number) {
    this.mainService.getSingle(id)
    .subscribe(res => {
      this.data = res;
    }, error => {
      console.log(error)
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Sucursal Agregado', 'El sucursal fue agregado exitosamente.');
      this.modalController.dismiss(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Sucursal Actualizado', 'El sucursal fue actualizado exitosamente.');
      this.modalController.dismiss(res);
      console.log(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

}
