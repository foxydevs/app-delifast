import { TestBed } from '@angular/core/testing';

import { CuentaCobrarService } from './cuenta-cobrar.service';

describe('CuentaCobrarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CuentaCobrarService = TestBed.get(CuentaCobrarService);
    expect(service).toBeTruthy();
  });
});
