import { TestBed } from '@angular/core/testing';

import { CompraTipoService } from './compra-tipo.service';

describe('CompraTipoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompraTipoService = TestBed.get(CompraTipoService);
    expect(service).toBeTruthy();
  });
});
