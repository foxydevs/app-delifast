import { TestBed } from '@angular/core/testing';

import { ProductoTipoService } from './producto-tipo.service';

describe('ProductoTipoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductoTipoService = TestBed.get(ProductoTipoService);
    expect(service).toBeTruthy();
  });
});
