import { TestBed } from '@angular/core/testing';

import { CuentaPagarService } from './cuenta-pagar.service';

describe('CuentaPagarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CuentaPagarService = TestBed.get(CuentaPagarService);
    expect(service).toBeTruthy();
  });
});
