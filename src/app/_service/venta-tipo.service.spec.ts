import { TestBed } from '@angular/core/testing';

import { VentaTipoService } from './venta-tipo.service';

describe('VentaTipoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VentaTipoService = TestBed.get(VentaTipoService);
    expect(service).toBeTruthy();
  });
});
