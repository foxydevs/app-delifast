import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ModalVentaComponent } from './venta/modal-venta/modal-venta.component';
import { ModalCompraComponent } from './compra/modal-compra/modal-compra.component';
import { ModalClienteComponent } from './cliente/modal-cliente/modal-cliente.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ProveedorComponent } from './proveedor/proveedor.component';
import { ModalProveedorComponent } from './proveedor/modal-proveedor/modal-proveedor.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ModalVentaComponent,
    ModalCompraComponent,
    ModalClienteComponent,
    ClienteComponent,
    ProveedorComponent,
    ModalProveedorComponent
  ],
  entryComponents: [
    ModalVentaComponent,
    ModalCompraComponent,
    ModalClienteComponent,
    ClienteComponent,
    ProveedorComponent,
    ModalProveedorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    FormsModule,
    IonicModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
