import { Component, OnInit } from '@angular/core';
import { NotificacionService } from '../_service/notificacion.service';
import { LoginService } from '../_service/login.service';
import { Router, Event } from '@angular/router';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private disabledBtn:boolean;
  private passwordType:string = 'password';
  private passwordShow:boolean = false;
  data = {
    username: '',
    password: ''
  }

  constructor(
    private notificacionService: NotificacionService,
    private mainService: LoginService,
    private router: Router,
    private events: Events
  ) { }

  ngOnInit() {
    localStorage.setItem('currentAuthentication', 'NoAuthentication');
  }

  authentication() {
    if(this.data.username) {
      if(this.data.password) {
        this.auth();
      } else {
        this.notificacionService.alertToast("La contraseña es requerida.");
      }
    } else {
      this.notificacionService.alertToast("El usuario es requerido.");
    }
  }

  auth() {
    //let events = this.events;
    this.disabledBtn = true;
    this.mainService.auth(this.data)
    .subscribe((res) => {
      console.log(res)
      localStorage.setItem('currentUser', res.username);
      localStorage.setItem('currentEmail', res.email);
      localStorage.setItem('currentFirstName', ((res.empleados?res.empleados.nombre:'')));
      localStorage.setItem('currentLastName', ((res.empleados?res.empleados.apellido:'')));
      localStorage.setItem('currentId', res.id);
      localStorage.setItem('currentPicture', res.picture);
      localStorage.setItem('currentState', res.estado);
      localStorage.setItem('currentRol', res.roles.descripcion);
      localStorage.setItem('currentRolId', res.rol);
      localStorage.setItem('currentAuthentication', 'Authentication');
      this.events.publish('user:login');
      this.router.navigate(['home']);
      this.disabledBtn = false;
    }, (error) => {
      console.error(error);
      if(error.status == 401) {
        this.disabledBtn = false;
        this.notificacionService.alertToast("Usuario o contraseña incorrectos.");
      }
      this.disabledBtn = false;
    });
  }

  //TOGGLE PASSWORD
  togglePassword() {
    if(this.passwordShow) {
      this.passwordShow = false;
      this.passwordType = 'password';
    } else {
      this.passwordShow = true;
      this.passwordType = 'text';
    }
  }

}
