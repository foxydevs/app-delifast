import { Component, OnInit } from '@angular/core';
import { ProveedorService } from '../_service/proveedor.service';
import { ModalController, AlertController } from '@ionic/angular';
import { NotificacionService } from '../_service/notificacion.service';
import { ModalProveedorComponent } from '../proveedor/modal-proveedor/modal-proveedor.component';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.page.html',
  styleUrls: ['./proveedores.page.scss'],
})
export class ProveedoresPage implements OnInit {
  table:any[];

  constructor(
    private mainService: ProveedorService,
    private modalController: ModalController,
    private alertController: AlertController,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.getAll();
  }

  closeModal() {
    this.modalController.dismiss();
  }

  getAll() {
    this.mainService.getAll()
    .subscribe((res) => {
      this.table = [];
      this.table = res;
      console.log(res);
    }, (error) => {
      console.log(error);
    })
  }

  async presentAlertConfirm(id:any) {
    const alert = await this.alertController.create({
      header: 'Eliminar',
      message: '¿Deseas eliminar el proveedor?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            console.log('Confirm Okay');
            this.delete(id)
          }
        }
      ]
    });

    await alert.present();
  }

  delete(id:any) {
    this.mainService.delete(id)
    .subscribe((res) => {
      this.notificationService.alertMessage('Proveedor Eliminado', 'El proveedor fue eliminado exitosamente.');
      this.getAll();
    }, (error) => {
      console.clear
    });
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalProveedorComponent,
      componentProps: {
        value: id
      }
    });

    modal.onDidDismiss().then((data: any) => {
      if(data.data) {
        this.getAll();
      }
    });

    return await modal.present();
  }

  getProveedor(data:any) {
    this.modalController.dismiss(data);
  }

}
