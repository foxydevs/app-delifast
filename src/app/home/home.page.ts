import { Component } from '@angular/core';
import { NotificacionService } from '../_service/notificacion.service';
import { VentaService } from '../_service/venta.service';
import { UsuarioService } from '../_service/usuario.service';
import { ModalController } from '@ionic/angular';
import { ModalVentaComponent } from '../venta/modal-venta/modal-venta.component';
import { CompraService } from '../_service/compra.service';
import { ModalCompraComponent } from '../compra/modal-compra/modal-compra.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  table:any[];
  usuarios:any[];
  selectItem:string = 'venta';
  data = {
    fechaInicio: '',
    fechaFin: '',
    usuario: 0,
    estado: 0,
  }
  constructor(
    private notificationService: NotificacionService,
    private mainService: VentaService,
    private secondService: UsuarioService,
    private thirdService: CompraService,
    private modalController: ModalController,
  ) {
    this.getAllSecond();
  }

  getAll = () => {
    if(this.data.fechaInicio) {
      if(this.data.fechaFin) {
        if(this.data.estado) {
          this.notificationService.alertLoading('Cargando...', 1000);
          this.mainService.getDate(this.data.usuario, this.data.fechaInicio.split("T")[0], this.data.fechaFin.split("T")[0])
          .subscribe((res)=>{
            this.table = [];
            res.forEach(element => {
              if(element.estado == +this.data.estado) {
                this.table.push(element)
              }
            });
            //console.log(res)
            /*this.table = res;
            this.table.reverse();*/
            this.notificationService.dismiss();
          }, (error) => {
            this.notificationService.dismiss();      
          });
        } else {
          this.notificationService.alertToast('El estado es requerido.');
        }
      } else {
        this.notificationService.alertToast('La fecha final es requerida.');
      }
    } else {
      this.notificationService.alertToast('La fecha de inicio es requerida.');
    }
  }

  getAllThird = () => {
    if(this.data.fechaInicio) {
      if(this.data.fechaFin) {
        if(this.data.estado) {
          this.notificationService.alertLoading('Cargando...', 1000);
          this.thirdService.getDate(this.data.usuario, this.data.fechaInicio.split("T")[0], this.data.fechaFin.split("T")[0])
          .subscribe((res)=>{
            this.table = [];
            res.forEach(element => {
              if(element.estado == +this.data.estado) {
                this.table.push(element)
              }
            });
            //console.log(res)
            /*this.table = res;
            this.table.reverse();*/
            this.notificationService.dismiss();
          }, (error) => {
            this.notificationService.dismiss();      
          });
        } else {
          this.notificationService.alertToast('El estado es requerido.');
        }
      } else {
        this.notificationService.alertToast('La fecha final es requerida.');
      }
    } else {
      this.notificationService.alertToast('La fecha de inicio es requerida.');
    }
  }

  getAllSecond = () => {
    this.secondService.getAll()
    .subscribe((res)=>{
      console.log(res)
      this.usuarios   = [];
      this.usuarios = res;
    }, (error) => {  
    });
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: ModalVentaComponent,
      componentProps: {
        value: id
      }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
    });
    return await modal.present();
  }

  async presentModalCompra(id:number) {
    const modal = await this.modalController.create({
      component: ModalCompraComponent,
      componentProps: {
        value: id
      }
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
    });
    return await modal.present();
  }
}
