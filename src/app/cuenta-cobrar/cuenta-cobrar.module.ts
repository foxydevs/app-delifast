import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CuentaCobrarPageRoutingModule } from './cuenta-cobrar-routing.module';

import { CuentaCobrarPage } from './cuenta-cobrar.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    CuentaCobrarPageRoutingModule
  ],
  declarations: [CuentaCobrarPage]
})
export class CuentaCobrarPageModule {}
