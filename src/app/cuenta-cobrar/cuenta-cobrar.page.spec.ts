import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CuentaCobrarPage } from './cuenta-cobrar.page';

describe('CuentaCobrarPage', () => {
  let component: CuentaCobrarPage;
  let fixture: ComponentFixture<CuentaCobrarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuentaCobrarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CuentaCobrarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
