import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CuentaCobrarPage } from './cuenta-cobrar.page';

const routes: Routes = [
  {
    path: '',
    component: CuentaCobrarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CuentaCobrarPageRoutingModule {}
