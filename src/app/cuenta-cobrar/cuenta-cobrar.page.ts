import { Component, OnInit } from '@angular/core';
import { NotificacionService } from '../_service/notificacion.service';
import { CuentaCobrarService } from '../_service/cuenta-cobrar.service';

@Component({
  selector: 'app-cuenta-cobrar',
  templateUrl: './cuenta-cobrar.page.html',
  styleUrls: ['./cuenta-cobrar.page.scss'],
})
export class CuentaCobrarPage implements OnInit {
  search:string;
  table:any[];
  selectItem:string = 'cuenta';

  constructor(
    private notificationService: NotificacionService,
    private mainService: CuentaCobrarService,
  ) { }

  ngOnInit() {
    this.getAll();
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
    if(ev.detail.value == 'cuenta') {
      this.getAll();
    } else if(ev.detail.value == 'pagada') {
      this.getAllPagadas();
    }
  }

  getAll = () => {
    this.notificationService.alertLoading('Cargando...', 1000);
    this.mainService.getAll()
    .subscribe((res)=>{
      console.log(res)
      this.table = [];
      this.table = res;
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();      
    });
  }

  getAllPagadas = () => {
    this.notificationService.alertLoading('Cargando...', 1000);
    this.mainService.getAllPagadas()
    .subscribe((res)=>{
      console.log(res)
      this.table = [];
      this.table = res;
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();      
    });
  }

}
