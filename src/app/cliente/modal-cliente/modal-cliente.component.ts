import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ClienteService } from 'src/app/_service/cliente.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-modal-cliente',
  templateUrl: './modal-cliente.component.html',
  styleUrls: ['./modal-cliente.component.scss'],
})
export class ModalClienteComponent implements OnInit {
  parameter:string;
  client = {
    nombre: '',
    apellido: '',
    direccion: '',
    nit: '',
    telefono: '',
    celular: '',
    id: ''
  }
  btnDisabled:boolean;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private mainService: ClienteService,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value')
    if(this.parameter) {
      this.getSingle(+this.parameter);
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

    //GUARDAR CAMBIOS
  saveChanges() {
    if(this.client.nombre) {
      if(this.client.apellido) {
        if(this.parameter) {
          this.btnDisabled = true;
          this.update(this.client);
        } else {
          this.btnDisabled = true;
          this.create(this.client);
        }
      } else {
        this.notificationService.alertToast('El apellido es requerida.');
      }
    } else {
      this.notificationService.alertToast('El nombre es requerido.');
    }
  }

  getSingle(id:number) {
    this.mainService.getSingle(id)
    .subscribe(res => {
      this.client = res;
    }, error => {
      console.log(error)
    })
  }

  //AGREGAR
  create(formValue:any) {
    this.mainService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Cliente Agregado', 'El cliente fue agregado exitosamente.');
      this.modalController.dismiss(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }

  //ACTUALIZAR
  update(formValue:any) {
    this.mainService.update(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Cliente Actualizado', 'El cliente fue actualizado exitosamente.');
      this.modalController.dismiss(res);
      console.log(res);
    }, (error) => {
      this.btnDisabled = false;
      console.clear
    });
  }
}
