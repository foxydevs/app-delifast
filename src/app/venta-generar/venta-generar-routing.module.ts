import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VentaGenerarPage } from './venta-generar.page';

const routes: Routes = [
  {
    path: '',
    component: VentaGenerarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VentaGenerarPageRoutingModule {}
