import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ProductoService } from 'src/app/_service/producto.service';
import { ProductoTipoService } from 'src/app/_service/producto-tipo.service';

@Component({
  selector: 'app-modal-venta-product',
  templateUrl: './modal-venta-product.component.html',
  styleUrls: ['./modal-venta-product.component.scss'],
})
export class ModalVentaProductComponent implements OnInit {
  private parameter:any;
  private products:any[] = [];
  private typesProducts:any[] = [];
  private select:boolean = true;
  private form:boolean = false;
  private precioClienteEs:boolean = false;
  private precioDistribuidor:boolean = false;
  private product = {
    id: '',
    codigo: '',
    nombre: '',
    descripcion: '',
    marca: '',
    tipo: '',
    producto: '',
    cantidad: 0,
    precioCosto: 0,
    precioVenta: 0,
    precioClienteEs: 0,
    precioDistribuidor: 0,
    subtotal: 0
  }

  constructor(
    public modalController: ModalController,
    private notificationService: NotificacionService,
    public productsService: ProductoService,
    public typesProductService: ProductoTipoService
    ) {
    this.loadProducts();
    this.loadTypesProducts();
  }

  ngOnInit() {}

  //CERRAR modalController
  public closeModal() {
    this.modalController.dismiss();
  }

  //CARGAR PRODUCTOS
  public loadProducts() {
    this.productsService.getAllExistencia()
    .subscribe(response => {
      this.products = response;
    }, (error) => {
      console.log(error)
    })
  }

  //CARGAR TIPOS DE PRODUCTO
  public loadTypesProducts() {
    this.typesProductService.getAll()
    .subscribe(response => {
      this.typesProducts = response;
    }, (error) => {
      console.log(error)
    })
  }

  public selectProduct(data:any) {
    console.log(data)
    this.select = false;
    this.form = true;
    this.product.id = data.id
    this.product.codigo = data.productos.codigo
    this.product.nombre = data.productos.nombre
    this.product.descripcion = data.productos.descripcion
    this.product.marca = data.productos.marcaDes
    this.product.tipo = data.productos.tipo
    this.product.producto = data.productos.id
    this.product.cantidad = 0
    this.product.precioClienteEs = data.precioClienteEs
    this.product.precioVenta = data.precioVenta
    this.product.precioDistribuidor = data.precioDistribuidor
    this.product.precioCosto = data.precioCosto
    this.precioClienteEs = true;
    this.precioDistribuidor = true;
  }

  public saveChanges() {
    this.product.subtotal = this.product.cantidad * this.product.precioVenta;
    this.modalController.dismiss(this.product);
  }

  public changePrice(){
    this.product.precioClienteEs = this.product.precioVenta-(this.product.precioVenta*0.15)
    this.product.precioDistribuidor = this.product.precioVenta-(this.product.precioVenta*0.20)
  }

}
