import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VentaGenerarPageRoutingModule } from './venta-generar-routing.module';

import { VentaGenerarPage } from './venta-generar.page';
import { ModalVentaProductComponent } from './modal-venta-product/modal-venta-product.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VentaGenerarPageRoutingModule
  ],
  declarations: [
    VentaGenerarPage,
    ModalVentaProductComponent
  ], entryComponents: [
    ModalVentaProductComponent
  ]
})
export class VentaGenerarPageModule {}
