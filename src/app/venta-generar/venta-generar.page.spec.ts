import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VentaGenerarPage } from './venta-generar.page';

describe('VentaGenerarPage', () => {
  let component: VentaGenerarPage;
  let fixture: ComponentFixture<VentaGenerarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentaGenerarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VentaGenerarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
