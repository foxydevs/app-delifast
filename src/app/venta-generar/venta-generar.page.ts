import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../_service/cliente.service';
import { VentaService } from '../_service/venta.service';
import { NotificacionService } from '../_service/notificacion.service';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { VentaTipoService } from '../_service/venta-tipo.service';
import { ClienteComponent } from '../cliente/cliente.component';
import { ModalVentaProductComponent } from './modal-venta-product/modal-venta-product.component';

@Component({
  selector: 'app-venta-generar',
  templateUrl: './venta-generar.page.html',
  styleUrls: ['./venta-generar.page.scss'],
})
export class VentaGenerarPage implements OnInit {
  typesSales:any[] = [];
  products:any[] = [];
  selectItem:String = 'detalle';
  purchase = {
    nit: '',
    nombre: '',
    direccion: '',
    cliente: '',
    fecha: '',
    comprobante: 0,
    tipo: '',
    tipoPlazo: '',
    plazo: '',
    total: 0,
    detalle: [],
    usuario: +localStorage.getItem('currentId')
  }
  public parameter:any;
  private now:any;

  constructor(
    public secondService: VentaTipoService,
    public proveedorService: ClienteService,
    public mainService: VentaService,
    public notificationService: NotificacionService,
    public modalController: ModalController,
    private router: Router,
    private location: Location
  ) {
    this.getAllTipoCompra();
    this.loadInvoice();
    let date = new Date();
    let month = date.getMonth()+1;
    let month2;
    let dia= date.getDate();
    let dia2;
    if(month<10){
      month2='0'+month;
    }else{
      month2=month
    }
    if(dia<10){
      dia2='0'+dia;
    }else{
      dia2=dia
    }
    this.now= date.getFullYear()+'-'+month2+'-'+dia2
    this.purchase.fecha = this.now;
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
  }

  ngOnInit() {
  }

  //CARGAR VENTAS
  public getAllTipoCompra(){
    this.secondService.getAll()
    .subscribe(res => {
      this.typesSales = res;
      console.log(this.typesSales)
    }, (error) => {
      console.clear
    });
  }

  //CARGAR PROVEEDOR
  public getSingleProvider(id:any) {
    this.proveedorService.getSingle(id)
    .subscribe(res => {
      this.purchase.cliente = res.id;
      this.purchase.nit = res.nit;
      this.purchase.direccion = res.direccion;
      this.purchase.nombre = res.nombre;
    }, (error) => {
      console.clear
    });
  }

  //CARGAR FACTURA
  public loadInvoice(){
    this.mainService.getComprobante()
    .subscribe(res => {
      console.log(res);
      this.purchase.comprobante = +res.comprobante + 1;//+num[0].id + 1;
    }, (error) => {
      console.clear
    })
  }

  async presentModalClient() {
    const modal = await this.modalController.create({
      component: ClienteComponent
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
      if(data.data) {
        this.getSingleProvider(data.data);
        this.purchase.tipo = '1';
      }
    });
    return await modal.present();
  }

  async presentModalProducts() {
    const modal = await this.modalController.create({
      component: ModalVentaProductComponent
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
      if(data.data) {
        let a:number = 0;
        let b:number = 0;
        this.parameter = data.data;
        this.products.push(this.parameter);
        for(let x of this.products) {
          a = x.subtotal;
          b += a;
        }
        this.purchase.total = b;
        console.log(this.products)
      }
    });
    return await modal.present();
  }

  //REMUEVE EL PRODUCTO
  public removeAddItem(e:any):any {
    this.purchase.total -= e.subtotal;
    this.products.splice(this.products.indexOf(e),1)
  }

  saveChanges() {
    this.purchase.detalle = this.products;
    if(this.purchase.detalle.length > 0) {
      if(this.purchase.nit) {
        if(this.purchase.tipo) {
          this.savePurchase();
        } else {
          this.notificationService.alertToast('El tipo es requerido,')
        }
      } else {
        this.notificationService.alertToast('El cliente es requerido.')
      }
    } else {
      this.notificationService.alertToast('El detalle es requerido.')
    }
  }

  //AGREGAR INVENTARIO
  public savePurchase() {
    if(this.purchase.detalle.length > 0) {
      this.mainService.create(this.purchase)
      .subscribe((res) => {
        //this.navCtrl.setRoot(PurchaseAdminPage)
        this.goToRoute('venta')
      }, (error) => {
        console.log(error)
      })
    } else {
      this.notificationService.alertToast('El detalle es requerido.')
    }
  }


}
